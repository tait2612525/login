import { NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-homepage',
  standalone: true,
  imports: [NgIf, RouterLink],
  templateUrl: './homepage.component.html',
  styleUrl: './homepage.component.scss',
})
export class HomepageComponent {
  public loggedIn = false;
  public userData: any;

  ngOnInit(): void {
    const userDataString = localStorage.getItem('user-Data');
    if (userDataString?.length) {
      try {
        this.userData = JSON.parse(userDataString);
        this.loggedIn = true;
      } catch (error) {
        this.loggedIn = false;
      }
    } else {
      this.loggedIn = false;
    }
  }

  logout(): void {
    this.loggedIn = false;
    localStorage.removeItem('user-Data');
  }
}
