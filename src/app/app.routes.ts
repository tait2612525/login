import { Routes } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';
import { HomepageComponent } from './homepage/homepage.component';

export const routes: Routes = [
  { path: 'login', component: LoginFormComponent },
  { path: '', component: HomepageComponent },
];
